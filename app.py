from flask import Flask
from flask import request


class App:
    app = Flask(__name__)

    @app.route('/')
    def index():
        return 'Hello Bob from xxxxxx'

    @app.route('/add')
    def add():
        a = request.args.get('a', "0")
        b = request.args.get('b', "0")
        return str(int(a)+int(b))

    @app.route('/sub')
    def sub():
        a = request.args.get('a', "0")
        b = request.args.get('b', "0")
        return str(int(a)-int(b))

    def run(self, host, port):
        self.app.run(host=host, port=port)


if __name__ == "__main__":
    a = App()
    a.run(host='0.0.0.0', port=8001)
