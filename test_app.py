import requests, unittest, app

url = "http://13.209.15.210:8001"

class TestApp(unittest.TestCase):
    
    def test_add(self):
        result = requests.get(url=url+"/add", params={'a': 2, 'b': 3}).text
        self.assertEqual(result, str(2+3))

    def test_sub(self):
        result = requests.get(url=url+"/sub", params={'a': 2, 'b': 3}).text
        self.assertEqual(result, str(2-3))

if __name__ == "__main__":
    unittest.main()
